﻿// C# example.
using UnityEditor;
using System.Diagnostics;
using UnityEngine;

public class ScriptBatch
{
    [MenuItem("My Tools/Build All APKs")]
    public static void BuildAllApks()
    {
        BuildTestDevice();
        BuildCarInfotainment();
        BuildCoffeeMachine();
        BuildMicrowave();
        BuildToaster();
        BuildRemote();
    }

    [MenuItem("My Tools/Build Test Device")]
    public static void BuildTestDevice()
    {
        BuildGame("Device");
    }

    [MenuItem("My Tools/Build Car Infotainment")]
    public static void BuildCarInfotainment()
    {
        BuildGame("CarInfotainment");
    }

    [MenuItem("My Tools/Build Coffee Machine")]
    public static void BuildCoffeeMachine()
    {
        BuildGame("CoffeeMachine");
    }

    [MenuItem("My Tools/Build Microwave")]
    public static void BuildMicrowave()
    {
        BuildGame("Microwave");
    }

    [MenuItem("My Tools/Build Toaster")]
    public static void BuildToaster()
    {
        BuildGame("Toaster");
    }

    [MenuItem("My Tools/Build Remote")]
    public static void BuildRemote()
    {
        BuildGame("Remote");
    }

    public static void BuildGame(string id)
    {
        // Get filename. 
        string path = Application.dataPath.Replace("/Assets", ""); // EditorUtility.SaveFolderPanel("APKs", "", "");
        UnityEngine.Debug.Log(path);
        string[] levels = new string[] { "Assets/Scenes/VivianARCoreTestScene" + id + ".unity" };

        PlayerSettings.productName = "VivianARTest" + id;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "de.ugoe.cs.vivian.arcore.Test" + id);

        // Build player.
        BuildPipeline.BuildPlayer(levels, path + "/ARCoreTest" + id + ".apk", BuildTarget.Android, BuildOptions.None);

        Process proc = new Process();
        proc.StartInfo.FileName = "C:/Program Files (x86)/Android/android-sdk/platform-tools/adb.exe";
        proc.StartInfo.Arguments = "-d install -r " + path + "/ARCoreTest" + id + ".apk";
        proc.Start();

        proc.Exited += Proc_Exited;
        proc.ErrorDataReceived += Proc_ErrorDataReceived;
        proc.OutputDataReceived += Proc_OutputDataReceived;

        proc.WaitForExit();
    }

    private static void Proc_Exited(object sender, System.EventArgs e)
    {
        UnityEngine.Debug.Log("apk installed");
        UnityEngine.Debug.Log(e.ToString());
    }

    private static void Proc_OutputDataReceived(object sender, DataReceivedEventArgs e)
    {
        UnityEngine.Debug.Log(e.Data);
    }

    private static void Proc_ErrorDataReceived(object sender, DataReceivedEventArgs e)
    {
        UnityEngine.Debug.Log(e.Data);
    }

}