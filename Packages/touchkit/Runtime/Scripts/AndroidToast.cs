﻿using System;
using UnityEngine;

public class AndroidToast : MonoBehaviour
{

    /// <summary>
    /// Creates a native Android Toast 
    /// </summary>
    /// <param name="toastString">The string to display</param>
    public static void ShowFeedback(string toastString)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");
            activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
                AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", toastString);
                AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>("LENGTH_SHORT"));
                toast.Call("show");
            }));
        }
    }

    /// <summary>
    /// Creates a native Android Toast with specific gravity and location
    /// </summary>
    /// <param name="toastString">The string to display</param>
    /// <param name="gravity">The constant for overall position of the string to display</param>
    /// <param name="X">X offset to the position defined by the Gravity constant</param>
    /// <param name="Y">Y offset to the position defined by the Gravity constant</param>
    /// <param name="lengthLong">Flag for choosing between short and long length</param>
    public static void ShowFeedback(string toastString, int gravity, int X, int Y, bool lengthLong)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");

            activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
/*              ========== OLD TOAST METHOD ==========
                AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
                AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", toastString);
                AndroidJavaObject toast;

                if (LENGTH_SHORT)
                    toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>("LENGTH_SHORT"));
                else
                    toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>("LENGTH_LONG"));

                toast.Call("setGravity", gravity, X, Y);
                toast.Call("show");*/

/*              ========== CHOCOBAR ==========
/*                AndroidJavaClass chocobarCls = new AndroidJavaClass("com.pd.chocobar.ChocoBar");
                AndroidJavaObject snackObj = chocobarCls.CallStatic<AndroidJavaObject>("builder");
                snackObj.Call<AndroidJavaObject>("setActivity", activity);
                snackObj.Call<AndroidJavaObject>("setTextSize", 20);
                snackObj.Call<AndroidJavaObject>("setText", javaString);
                snackObj.Call<AndroidJavaObject>("orange");
                snackObj.Call("show");*/

                AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", toastString);
                AndroidJavaObject toastyCls = new AndroidJavaObject("es.dmoral.toasty.Toasty");
                toastyCls.SetStatic("textSize", 22);
                AndroidJavaObject toastObj = toastyCls.CallStatic<AndroidJavaObject>("warning", context, javaString,
                    toastyCls.GetStatic<int>(lengthLong ? "LENGTH_LONG" : "LENGTH_SHORT"), true);
                toastObj.Call("setGravity", gravity, X, Y);
                toastObj.Call("show");
                toastyCls.Dispose();
                toastObj.Dispose();

            }));
        }
    }

    #region Gravity Constants
    /* Gravity constants can be combined with bitwise OR operator
     * For example: TOP | CENTER_HORIZONTAL
     */
    public static class Gravity
    {
        private static AndroidJavaObject AndroidGravity = new AndroidJavaClass("android.view.Gravity");

        public static int BOTTOM = AndroidGravity.GetStatic<int>("BOTTOM");
        public static int CENTER = AndroidGravity.GetStatic<int>("CENTER");
        public static int CENTER_HORIZONTAL = AndroidGravity.GetStatic<int>("CENTER_HORIZONTAL");
        public static int CENTER_VERTICAL = AndroidGravity.GetStatic<int>("CENTER_VERTICAL");
        public static int LEFT = AndroidGravity.GetStatic<int>("LEFT");
        public static int RIGHT = AndroidGravity.GetStatic<int>("RIGHT");
        public static int TOP = AndroidGravity.GetStatic<int>("TOP");
    }

    #endregion
}