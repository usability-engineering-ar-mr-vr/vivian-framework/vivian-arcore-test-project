using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using de.ugoe.cs.vivian.core;

public class TouchKitConfigurator : MonoBehaviour
{
    /*============================== Parameters ==============================*/
    #region Secret Pattern and Gesture Detection Parameters

    /* Secret pattern parameters */
    // Lower bound of the scene reset pattern region
    private const int RESET_REGION_START = 0;
    // Upper bound of the scene reset pattern region
    private const int RESET_REGION_LIMIT = 125;
    // Number of taps required for scene reset
    private const int RESET_TAPS = 7;
    // Resets the scene once this counter reaches RESET_TAPS
    private int sceneResetCounter = 0;

    /* Logging pattern parameters */
    // NOTE: The logging triggering region is currently not optimal
    // Lower bound of the logging trigger region
    private const int LOGS_REGION_START = 126;
    // Upper bound of the logging trigger region
    private const int LOGS_REGION_LIMIT = 250;
    // Number of taps required for triggering the logging mode (aka sending longs to the server)
    private const int START_LOGGING_TAPS = 7;
    // Starts the logging mode once this counter reaches START_LOGGING_TAPS
    private int logActivateCounter = 0;
    // Activates once the required taps are completed in the right region
    private bool logging_mode = false;

    /* Pinch-zoom Recognizer parameters */
    // Minimum delta scale required to detect Pinch-zoom gesture
    private const float MINIMUM_DELTA_SCALE = 0.10f;

    /* Rotation Recognizer parameters */
    // Minimum angle rotation delta required to detect Rotation gesture
    private const float MINIMUM_ANGLE_ROTATION_DELTA = 0.15f;
    #endregion

    #region Feedback helpers
    /* Feedback type 0 - no feedback at all
     * Feedback type 1 - feedback for unallowed gestures
     * Feedback type 2 - Feedback type 1 + advice 
     * Special  type 3 - New feedback with better visuals and custom game object feedback
     * Special  type 4 - Case where a message for all gestures is given (debugging mode)
    */
    private int FEEDBACK_TYPE = 3;

    // NOTE: Each gesture is logged, when the logging mode is turned ON, no matter if the user receives a feedback or not.
    // Get each gesture (aka recognizer) and log it, then generate appropriate feedback according to FEEDBACK_TYPE 
    private string logAndGenerateFeedbackMessage(TKAbstractGestureRecognizer recognizer, bool movableElementTouched)
    {
        string feedback = null;

        // Validate the parameter
        if (recognizer == null)
        {
            //Debug.Log("Recognizer is null![2]");
            return null;
        }

        if (FEEDBACK_TYPE < 0 || FEEDBACK_TYPE > 4)
        {
            //Debug.Log("FEEDBACK_TYPE is out of bounds!");
            return null;
        }

        // Allowed gestures will not show any messages (unless in debugging mode, type 3)
        // Unallowed gestures will give feedback according to the FEEDBACK_TYPE parameter
        if (recognizer is TKTapRecognizer)
        {
            //Debug.Log("Recognizer is tap.");
            feedback = "Tap completed."; // Debug mode message
            // if (logging_mode) AutoQUESTMonitor.LogEvent(feedback);
            if (FEEDBACK_TYPE == 4) return feedback; else return null;
        }
        else if (recognizer is TKPanRecognizer)
        {
            //Debug.Log("Recognizer is pan.");
            feedback = "Pan completed."; // Debug mode message
            // if (logging_mode) AutoQUESTMonitor.LogEvent(feedback);
            if (FEEDBACK_TYPE == 4) return feedback; else return null;
        }
        else if (recognizer is TKCurveRecognizer)
        {
            //Debug.Log("Recognizer is curve.");
            feedback = "Curve completed."; // Debug mode message
            // if (logging_mode) AutoQUESTMonitor.LogEvent(feedback);
            if (FEEDBACK_TYPE == 4) return feedback; else return null;
        }
        else if (recognizer is TKPinchRecognizer)
        {
            //Debug.Log("Recognizer is pinch.");
            if (FEEDBACK_TYPE == 0 || FEEDBACK_TYPE == 4)
            {
                feedback = "Pinch completed."; // Debug mode message
                // if (logging_mode) AutoQUESTMonitor.LogEvent(feedback);
                if (FEEDBACK_TYPE == 4) return feedback; else return null; // type 0
            }
            else // Either type 1, type 2 or type 3
            {
                if (FEEDBACK_TYPE == 1) feedback = "Zooming is not possible.";
                if (FEEDBACK_TYPE == 2) feedback = "Zooming is not possible. Try moving closer to the object.";
                if (FEEDBACK_TYPE == 3) feedback = "Step closer to enlarge on-screen objects";

                // if (logging_mode) AutoQUESTMonitor.LogEvent(feedback);
                return feedback;
            }
        }
        else if (recognizer is TKRotationRecognizer)
        {
            //Debug.Log("Recognizer is rotation.");
            if (FEEDBACK_TYPE == 0 || FEEDBACK_TYPE == 4)
            {
                feedback = "Rotation completed."; // Debug mode message
                // if (logging_mode) AutoQUESTMonitor.LogEvent(feedback);
                if (FEEDBACK_TYPE == 4) return feedback; else return null; // type 0
            }
            else // Either type 1, type 2 or type 3;
            {
                if (FEEDBACK_TYPE == 1) feedback = "Rotating the object is not possible.";
                if (FEEDBACK_TYPE == 2) feedback = "Rotating the object is not possible. Try moving around the object.";
                if (FEEDBACK_TYPE == 3 && !movableElementTouched) feedback = "Move around the objects";
                else feedback = "Hold the object and move the phone to rotate";

                // if (logging_mode) AutoQUESTMonitor.LogEvent(feedback);
//                if (logging_mode) AutoQUESTMonitor.LogEvent(feedback, this.gameObject, new KeyValuePair<string, string>("key", "value"));
                return feedback;
            }
        }

        // The code usually should not reach here, if this is the case, then either the recognizer was not detected 
        // or there is another problem
        //Debug.Log("Something is wrong!");
        return feedback;
    }

    // Show a generated feedback to the user and log if logging mode is turned on
    private void showAndLogFeedback(TKAbstractGestureRecognizer recognizer, bool movableElementTouched)
    {
        // Validate the parameter
        if (recognizer == null)
        {
            //Debug.Log("Recognizer is null![1]");
            return;
        }

        string feedback = logAndGenerateFeedbackMessage(recognizer, movableElementTouched);

        // if this is not null then FEEDBACK_TYPE parameter allows this feedback to be showed
        if (feedback != null) AndroidToast.ShowFeedback(feedback, AndroidToast.Gravity.TOP, 0, 0, true);
    }

    // Show feedback, which is not dependent on anything
    // Can be used to show info feedback about activation of secret patterns
    public void showAndLogFeedback(string feedback)
    {
        // Validate the parameter
        if (feedback == null)
        {
            //Debug.Log("Feedback is null!");
            return;
        }

        // if (logging_mode) AutoQUESTMonitor.LogEvent(feedback);
        AndroidToast.ShowFeedback(feedback, AndroidToast.Gravity.CENTER_HORIZONTAL, 0, 0, true);
    }

    private bool checkIfMovableElementTouched(TKAbstractGestureRecognizer recognizer)
    {
        RaycastHit hit;
        foreach (TKTouch touch in recognizer.getListTouches())
        {
            Ray ray = cam.ScreenPointToRay(new Vector3(touch.position.x, touch.position.y, 0));
            if (Physics.Raycast(ray.origin, ray.direction, out hit))
            {
                if(hit.transform.GetComponent<MovableElement>() != null)
                    return true;
            }
        }
        return false;
    }

    #endregion

    #region Secret patterns
    private void checkForSceneResetPattern(Vector2 touchLocation)
    {
        // Check if the tap is in the scene reset rectangle and if so increase the counter
        if (touchLocation.x > RESET_REGION_START && touchLocation.x < RESET_REGION_LIMIT &&
            touchLocation.y > RESET_REGION_START && touchLocation.y < RESET_REGION_LIMIT)
        {
            ++sceneResetCounter;
            // if the required amount of taps is reached
            if (sceneResetCounter == RESET_TAPS)
            {
                //Debug.Log("You have activated a secret pattern. The scene will be reset.");
                // Show secret pattern message and log it
                showAndLogFeedback("You have activated a secret pattern. The scene will be reset.");

                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                sceneResetCounter = 0;
            }
        }
        else // tap occured outside of the region, so reset the counter
        {
            sceneResetCounter = 0;
        }
    }
    private void checkForLoggingModePattern(Vector2 touchLocation)
    {
        // Check if the tap is in the log activating region
        if (touchLocation.x > LOGS_REGION_START && touchLocation.x < LOGS_REGION_LIMIT &&
            touchLocation.y > LOGS_REGION_START && touchLocation.y < LOGS_REGION_LIMIT)
        {
            ++logActivateCounter;
            // if the required amount of taps is reached and the logging mode is off
            if (logActivateCounter == START_LOGGING_TAPS && !logging_mode)
            {
                //Debug.Log("You have activated a secret pattern. Logging mode is turned ON.");
                // Show secret pattern message and log it
                showAndLogFeedback("You have activated a secret pattern. Logging mode is turned ON.");

                logging_mode = true;
                logActivateCounter = 0;
            }
        }
        else // tap occured outside of the region, so reset the counter
        {
            logActivateCounter = 0;
        }
    }
    #endregion

    [SerializeField]
    // The 1st person Camera
    private Camera cam = null;
    //The instance of AutoQUESTMonitor
    // private AutoQUESTGenericMonitorUnity AutoQUESTMonitor;
    private bool feedbackFlagPinch = false;
    private bool feedbackFlagRotation = false;

    void Start()
    {
        // AutoQUESTMonitor = AutoQUESTGenericMonitorUnity.getInstance();

        /*============================== Unallowed Gestures ==============================*/
        #region Unallowed Gestures

        // Pinch-zoom recognizer
        TKPinchRecognizer pinchRecognizer = new TKPinchRecognizer();
        // Rotation recognizer
        TKRotationRecognizer rotationRecognizer = new TKRotationRecognizer();
        
        rotationRecognizer.minimumRotationToRecognize = MINIMUM_ANGLE_ROTATION_DELTA;
        rotationRecognizer.gestureRecognizedEvent += (r) =>
        {
            float deltaRotation = rotationRecognizer.deltaRotation;

            if (Mathf.Abs(deltaRotation) >= MINIMUM_ANGLE_ROTATION_DELTA)
            {
                pinchRecognizer.state = TKGestureRecognizerState.FailedOrEnded;
                bool movableElementTouched = checkIfMovableElementTouched(rotationRecognizer);

                string debug = string.Format("DEBUG> S:{0}, C:{1}, DtR:{2}", rotationRecognizer.startTouchLocation(), rotationRecognizer.touchLocation(), deltaRotation);
                //Debug.Log("Rotation fired! " + debug);

                if (!feedbackFlagRotation)
                {
                    showAndLogFeedback(rotationRecognizer, movableElementTouched);
                    feedbackFlagRotation = true;
                }
            }
        };
        rotationRecognizer.gestureCompleteEvent += (r) => { feedbackFlagRotation = false; };
        
        pinchRecognizer.minimumScaleDistanceToRecognize = MINIMUM_DELTA_SCALE;
        pinchRecognizer.gestureRecognizedEvent += (r) =>
        {
            float deltaScale = pinchRecognizer.deltaScale;

            if (Mathf.Abs(deltaScale) > MINIMUM_DELTA_SCALE)
            {
                rotationRecognizer.state = TKGestureRecognizerState.FailedOrEnded;
                Vector2 startLocation = pinchRecognizer.startTouchLocation();
                Vector2 touchLocation = pinchRecognizer.touchLocation();

                string debug = string.Format("DEBUG> S:{0}, C:{1}, DtS:{2}", startLocation, touchLocation, deltaScale);
                //Debug.Log("Pinch fired! " + debug);

                if (!feedbackFlagPinch)
                {
                    showAndLogFeedback(pinchRecognizer, false);
                    feedbackFlagPinch = true;
                }
            }
        };
        pinchRecognizer.gestureCompleteEvent += (r) => { feedbackFlagPinch = false; };
        
        TouchKit.addGestureRecognizer(pinchRecognizer);
        TouchKit.addGestureRecognizer(rotationRecognizer);

        #endregion

        /*============================== Allowed Gestures ==============================*/
        #region Allowed Gestures

        // Tap recognizer + Hidden Pattern for Scene Reset
        TKTapRecognizer tapRecognizer = new TKTapRecognizer();
        tapRecognizer.gestureRecognizedEvent += (r) =>
        {
            // Since the touchLocation method returns the centroid location of all touches, 
            // the accuracy of the required touch location in case of many touches is under question. 
            Vector2 touchLocation = tapRecognizer.touchLocation();
            //Debug.Log("Tap completed: " + touchLocation);

            showAndLogFeedback(tapRecognizer, false);

            checkForLoggingModePattern(touchLocation);
            checkForSceneResetPattern(touchLocation);

        };
        TouchKit.addGestureRecognizer(tapRecognizer);

        // Curve recognizer
        TKCurveRecognizer curveRecognizer = new TKCurveRecognizer();
        curveRecognizer.gestureCompleteEvent += (r) =>
        {
            //Debug.Log("Curve completed: " + r);
            showAndLogFeedback(curveRecognizer, false);
        };
        TouchKit.addGestureRecognizer(curveRecognizer);

        // Pan recognizer
        TKPanRecognizer panRecognizer = new TKPanRecognizer();
        panRecognizer.gestureCompleteEvent += r =>
        {
            //Debug.Log("Pan completed: " + r);
            showAndLogFeedback(panRecognizer, false);
        };
        TouchKit.addGestureRecognizer(panRecognizer);

        #endregion
    }
}
